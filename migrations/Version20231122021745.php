<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122021745 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE "chat_messages_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "chat_rooms_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "users_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE "chat_messages" (id INT NOT NULL, author_id INT NOT NULL, room_id INT NOT NULL, uuid UUID NOT NULL, content TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EF20C9A6D17F50A6 ON "chat_messages" (uuid)');
        $this->addSql('CREATE INDEX IDX_EF20C9A6F675F31B ON "chat_messages" (author_id)');
        $this->addSql('CREATE INDEX IDX_EF20C9A654177093 ON "chat_messages" (room_id)');
        $this->addSql('COMMENT ON COLUMN "chat_messages".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "chat_messages".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE "chat_rooms" (id INT NOT NULL, uuid UUID NOT NULL, title VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7DDCF70DD17F50A6 ON "chat_rooms" (uuid)');
        $this->addSql('COMMENT ON COLUMN "chat_rooms".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "chat_rooms".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE chat_room_user (chat_room_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(chat_room_id, user_id))');
        $this->addSql('CREATE INDEX IDX_C87A2E561819BCFA ON chat_room_user (chat_room_id)');
        $this->addSql('CREATE INDEX IDX_C87A2E56A76ED395 ON chat_room_user (user_id)');
        $this->addSql('CREATE TABLE "users" (id INT NOT NULL, uuid UUID NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9D17F50A6 ON "users" (uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9F85E0677 ON "users" (username)');
        $this->addSql('COMMENT ON COLUMN "users".uuid IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN "users".created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE "chat_messages" ADD CONSTRAINT FK_EF20C9A6F675F31B FOREIGN KEY (author_id) REFERENCES "users" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "chat_messages" ADD CONSTRAINT FK_EF20C9A654177093 FOREIGN KEY (room_id) REFERENCES "chat_rooms" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat_room_user ADD CONSTRAINT FK_C87A2E561819BCFA FOREIGN KEY (chat_room_id) REFERENCES "chat_rooms" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chat_room_user ADD CONSTRAINT FK_C87A2E56A76ED395 FOREIGN KEY (user_id) REFERENCES "users" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE "chat_messages_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "chat_rooms_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE "users_id_seq" CASCADE');
        $this->addSql('ALTER TABLE "chat_messages" DROP CONSTRAINT FK_EF20C9A6F675F31B');
        $this->addSql('ALTER TABLE "chat_messages" DROP CONSTRAINT FK_EF20C9A654177093');
        $this->addSql('ALTER TABLE chat_room_user DROP CONSTRAINT FK_C87A2E561819BCFA');
        $this->addSql('ALTER TABLE chat_room_user DROP CONSTRAINT FK_C87A2E56A76ED395');
        $this->addSql('DROP TABLE "chat_messages"');
        $this->addSql('DROP TABLE "chat_rooms"');
        $this->addSql('DROP TABLE chat_room_user');
        $this->addSql('DROP TABLE "users"');
    }
}
