<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ChatMessage;
use App\Entity\ChatRoom;
use App\Entity\User;
use App\Form\ChatMessageType;
use App\Form\ChatRoomType;
use App\Repository\ChatMessageRepository;
use App\Repository\ChatRoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\UX\Turbo\TurboBundle;

class ChatRoomController extends AbstractController
{
    public function __construct(
        private readonly ChatRoomRepository $chatRoomRepository,
        private readonly ChatMessageRepository $chatMessageRepository,
        private readonly UrlGeneratorInterface $urlGenerator,
    ) {
    }

    #[Route(path: '/u/chats', name: 'chat_rooms', methods: ['GET', 'POST'])]
    public function index(#[CurrentUser] ?User $user, Request $request): Response
    {
        if (null === $user) {
            $this->denyAccessUnlessGranted('ROLE_USER');
        }

        $chatRoom = new ChatRoom();
        $form = $this->createForm(ChatRoomType::class, $chatRoom)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $chatRoom->addMember($user);
            $this->chatRoomRepository->save($chatRoom, flush: true);

            return $this->redirectToRoute('chat_room', ['uuid' => $chatRoom->getUuid()]);
        }

        return $this->render('chat_room/index.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/u/chats/{uuid}', name: 'chat_room', methods: ['GET', 'POST'])]
    public function show(ChatRoom $chatRoom, Request $request, HubInterface $hub, #[CurrentUser] ?User $user): Response
    {
        if (null === $user) {
            $this->denyAccessUnlessGranted('ROLE_USER');
        }

        $message = new ChatMessage();
        $form = $this->createForm(ChatMessageType::class, $message);
        $emptyForm = clone $form;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message->setAuthor($user);
            $message->setRoom($chatRoom);
            $this->chatMessageRepository->save($message, flush: true);

            if (TurboBundle::STREAM_FORMAT === $request->getPreferredFormat()) {
                $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
                $hub->publish(
                    new Update(
                        topics: $this->urlGenerator->generate('chat_room', ['uuid' => $chatRoom->getUuid()], UrlGeneratorInterface::ABSOLUTE_URL),
                        data: $this->renderView('components/message.stream.html.twig', [
                            'content' => $message->getContent(),
                            'author' => $message->getAuthor()->getUsername(),
                            'publish_date' => $message->getCreatedAt()->format('d/m/y H:i'),
                            'form' => $emptyForm,
                        ])
                    )
                );
            }

            return $this->redirectToRoute('chat_room', ['uuid' => $chatRoom->getUuid()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('chat_room/show.html.twig', [
            'form' => $form,
            'chatRoom' => $chatRoom,
        ]);
    }
}