import { startStimulusApp } from '@symfony/stimulus-bridge';

export const app = startStimulusApp(import.meta.webpackContext(
    '@symfony/stimulus-bridge/lazy-controller-loader!./controllers', {
        recursive: true,
        mode: 'lazy',
        regExp: /\.[jt]sx?$/
    },
));

// app.register('some_controller_name', SomeImportedController);
