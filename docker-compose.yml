version: "3.9"

services:
  # ------------------------------------------------------
  # PHP service (used only as dependency)
  # ------------------------------------------------------
  php:
    build:
      context: .
      dockerfile: docker/php/Dockerfile
    container_name: symfony_realtime_php
    networks:
      - default
    restart: unless-stopped
    volumes:
      - ./:/var/www/html
    working_dir: /var/www/html

  # ------------------------------------------------------
  # PHP service (used only for running processes)
  # ------------------------------------------------------
  infinite:
    build:
      context: .
      dockerfile: docker/php/Dockerfile
    container_name: symfony_realtime_infinite
    entrypoint: "tail -f /dev/null"
    networks:
      - default
    restart: unless-stopped
    volumes:
      - ./:/var/www/html
    working_dir: /var/www/html

  # ------------------------------------------------------
  # Node (used for frontend tasks)
  # ------------------------------------------------------
  node:
    build:
      context: .
      dockerfile: docker/node/Dockerfile
    container_name: symfony_realtime_node
    entrypoint: "tail -f /dev/null"
    networks:
      - default
    restart: unless-stopped
    volumes:
      - ./:/var/www/html
    working_dir: /var/www/html

  # ------------------------------------------------------
  # Exposed webserver
  # ------------------------------------------------------
  web:
    build:
      context: .
      dockerfile: docker/php/Dockerfile
    container_name: symfony_realtime_web
    depends_on:
      - php
      - database
    labels:
      caddy: "symfony-realtime.aaa"
      caddy.tls: internal
      caddy.reverse_proxy: "{{upstreams 80}}"
    networks:
      - default
      - global_reverse_proxy
    restart: unless-stopped
    volumes:
      - ./:/var/www/html
    working_dir: /var/www/html

  # ------------------------------------------------------
  # Postgresql database
  # ------------------------------------------------------
  database:
    container_name: symfony_realtime_database
    environment:
      POSTGRES_USER: "symfony_realtime"
      POSTGRES_PASSWORD: "symfony_realtime"
      POSTGRES_DB: "symfony_realtime"
    image: postgres:16.0
    networks:
      - default
    restart: unless-stopped
    volumes:
      - postgres_data:/var/lib/postgresql/data

  # ------------------------------------------------------
  # Database visualizer
  # ------------------------------------------------------
  adminer:
    container_name: symfony_realtime_adminer
    depends_on:
      - database
    image: adminer
    labels:
      caddy: "db.symfony-realtime.aaa"
      caddy.tls: internal
      caddy.reverse_proxy: "{{upstreams 8080}}"
    networks:
      - default
      - global_reverse_proxy
    restart: unless-stopped
  
  # ------------------------------------------------------
  # Email infrastructure
  # ------------------------------------------------------
  mailer:
    container_name: symfony_realtime_mailer
    image: maildev/maildev
    labels:
      caddy: "webmail.symfony-realtime.aaa"
      caddy.tls: internal
      caddy.reverse_proxy: "{{upstreams 1080}}"
    networks:
      - default
      - global_reverse_proxy
    restart: unless-stopped

  # ------------------------------------------------------
  # Cache infrastructure
  # ------------------------------------------------------
  cache:
    container_name: symfony_realtime_cache
    image: redis:7.2
    networks:
      - default
    restart: unless-stopped

  # ------------------------------------------------------
  # Realtime infrastructure
  # ------------------------------------------------------
  mercure:
    # Comment the following line to enable the production mode
    command: /usr/bin/caddy run --config /etc/caddy/Caddyfile.dev
    container_name: symfony_realtime_mercure
    environment:
      SERVER_NAME: ":80"
      MERCURE_PUBLISHER_JWT_KEY: '!ChangeThisMercureHubJWTSecretKey!'
      MERCURE_SUBSCRIBER_JWT_KEY: '!ChangeThisMercureHubJWTSecretKey!'
      MERCURE_EXTRA_DIRECTIVES: |-
        cors_origins "https://symfony-realtime.aaa"
        anonymous
        demo
    image: dunglas/mercure
    labels:
      caddy: mercure.symfony-realtime.aaa
      caddy.tls: internal
      caddy.reverse_proxy: "{{upstreams 80}}"
    networks:
      - default
      - global_reverse_proxy
    restart: unless-stopped
    volumes:
      - mercure_data:/data
      - mercure_config:/config

volumes:
  postgres_data:
    name: symfony_realtime_postgres
  mercure_data:
    name: symfony_realtime_mercure_data
  mercure_config:
    name: symfony_realtime_mercure_config

networks:
  default:
    driver: bridge
  global_reverse_proxy:
    external: true
